#include "power.h"
#include <cassert>

int
main() {
    assert(power(0, 0) == 1);    // Возведение в степень нуля.
    assert(power(0, 1) == 0);
    assert(power(2, 0) == 1);    // Типичные случаи.
    assert(power(2, 1) == 2);
    assert(power(2, 3) == 8);
    assert(power(-1, 0) == 1);   // Отрицательное основание.
    assert(power(-1, 2) == 1);
    assert(power(-1, 3) == -1);
}
