#include "power.h"

int
power(int x, unsigned int n) {
    if (n == 0) {
        return 1;
    }
    if (n % 2 == 0) {
        return power(x, n / 2) * power(n, n / 2);
    }
    return x * power(x, n - 1);
}
